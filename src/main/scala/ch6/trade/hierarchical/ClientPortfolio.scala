// Listing 6.22 A DSL implementation of Portfolio

package ch6.trade.hierarchical

import ch6.trade.Account
import ch6.trade.Currencies.HKD

trait ClientPortfolio extends Portfolio {
  val bal = new BalancesImpl
  import bal._
  import ch6.trade.Util.TODAY

  override def currentPortfolio(account: Account) = {

    // lookup database to get the amount credited for this account &
    // the currency in which it is booked
    val amount = 10000 //.. stubbed
    val ccy = HKD //.. stubbed
    val asOfDate = TODAY

    balance(amount, ccy, asOfDate)
  }
}

object ClientPortfolio extends ClientPortfolio
