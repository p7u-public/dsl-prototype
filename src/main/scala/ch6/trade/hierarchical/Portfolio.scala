// Listing 6.21 The Portfolio DSL contract

package ch6.trade.hierarchical

import ch6.trade.Account

trait Portfolio {
  val bal: Balances
  import bal._

  def currentPortfolio(account: Account): Balance
}

