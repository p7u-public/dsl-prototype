// Listing 6.16 DSL Composition

package ch6.trade.composition

import ch6.trade.Accounts.NOMURA
import ch6.trade.Currencies.USD
import ch6.trade.Instruments.IBM
import ch6.trade.Markets.NYSE
import ch6.trade.{ Logger, Mailer, TradeImplicits }
import scala.language.postfixOps

object EquityTradeMarketRuleDsl extends MarketRuleDsl {
  val semantics = EquityTradeDsl
}

object FixedIncomeTradeMarketRuleDsl extends MarketRuleDsl {
  val semantics = FixedIncomeTradeDsl
}

object ComposedDsl {
  def main(args: Array[String]): Unit = {
    import FixedIncomeTradeMarketRuleDsl._
    import TradeImplicits._

    val user = "john"

    // Listing 6.18 The Trade Lifecycle DSL

    withTrade(
      200 discount_bonds IBM
        for_client NOMURA
        on NYSE
        at 72.ccy(USD)) { trade =>
        Mailer(user) mail trade
        Logger() log trade
      } cashValue
  }
}
