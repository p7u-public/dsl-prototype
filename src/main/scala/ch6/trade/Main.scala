package ch6.trade

import ch6.trade.FixedIncomeTradingService.cashValue

object Main {

  import Accounts._
  import Currencies._
  import Instruments._
  import Markets._
  import TradeImplicits._

  val fixedIncomeTrade =
    200 discount_bonds IBM for_client NOMURA on NYSE at 72.ccy(USD)

  def main(args: Array[String]): Unit = {
    println(cashValue(200 discount_bonds IBM for_client NOMURA on NYSE at 72.ccy(USD)))
  }

}
