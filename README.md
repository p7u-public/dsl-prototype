dsl-prototype
=========

A demonstration on how to build a DSL.
Inspired from the book 'DSLs in action' by Debasish Ghosh.

(attempted to upgrade to scala 3, corrected some minor errors related to implicits - explicit return type required -, 
but then got stuck with an error in Composition.scala:30:6  "200 discount_bonds IBM" - is the RichInt implicit not
correctly resolved?)
